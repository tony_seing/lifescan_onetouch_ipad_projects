var gulp = require('gulp');
var watch = require('gulp-watch');
var slim = require("gulp-slim");
var connect = require('gulp-connect');

//var livereload = require('gulp-livereload');


gulp.task('slim', function() {
  gulp.src("slim/*.slim")
    .pipe(watch('slim/*.slim'))
    .pipe(slim({
      pretty: true
    }))
    .pipe(gulp.dest("./"));
});

gulp.task('server', function() {
   connect.server({
       root: '',
       livereload: true
  });
});
